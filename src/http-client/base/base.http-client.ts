import axios, { AxiosResponse } from 'axios';

export class BaseHttpClient {
  
  /* Token */

  private static authToken: string = '';
  private static refreshToken: string = '';

  constructor(protected baseUrl: string) {}

  protected async get<TResponse>(methodeUrl: string): Promise<TResponse> {
    return axios
      .get<any, AxiosResponse<TResponse>>(this.baseUrl + methodeUrl, { withCredentials: true, headers: this.getHeaders() })
      .then((res) => <TResponse>res.data);
  }

  protected async post<TRequest, TResponse>(methodeUrl: string, data: TRequest) {
    return axios
      .post<any, AxiosResponse<TResponse>>(this.baseUrl + methodeUrl, data, { withCredentials: true, headers: this.getHeaders() })
      .then((res) => <TResponse>res.data);
  }

  protected async put<TRequest, TResponse>(methodeUrl: string, data: TRequest) {
    return axios
      .put<any, AxiosResponse<TResponse>>(this.baseUrl + methodeUrl, data, { withCredentials: true, headers: this.getHeaders() })
      .then((res) => <TResponse>res.data);
  }

  protected async delete<T>(methodeUrl: string) {
    return axios
      .delete<any, AxiosResponse<T>>(this.baseUrl + methodeUrl, { withCredentials: true, headers: this.getHeaders() })
      .then((res) => <T>res.data);
  }

  private getHeaders(): any {
    const authToken: string = BaseHttpClient.getAuthToken();
    const refreshToken: string = BaseHttpClient.getRefreshToken();

    const headers: any = {
      'content-type': 'application/json',
      accept: 'application/json, text/plain, */*',
    };

    if (authToken != null && authToken != '') {
      headers.authorization = authToken;
    }

    if (refreshToken != null && refreshToken != '') {
      headers.refresh = refreshToken;
    }

    return headers;
  }

  /* Get & Set of tokens */

  public static getAuthToken(): string {
    return this.authToken;
  }

  public static setAuthToken(token: string): void {
    this.authToken = token;
  }

  public static getRefreshToken(): string {
    return this.refreshToken;
  }

  public static setRefreshToken(token: string): void {
    this.refreshToken = token;
  }
}
