import { AuthenticationDatasource } from './datasources/authentication.datasource';
import { BranchDatasource } from './datasources/branch.datasource';
import { FeedbackDatasource } from './datasources/feedback.datasource';
import { NewsDatasource } from './datasources/news.datasource';
import { StampDatasource } from './datasources/stamp.datasource';

export class HttpClient {
  /* Datasources */

  public authentication: AuthenticationDatasource;
  public branch: BranchDatasource;
  public stamp: StampDatasource;
  public news: NewsDatasource;
  public feedback: FeedbackDatasource;

  /* Init Datasources */

  constructor(baseUrl: string) {
    this.authentication = new AuthenticationDatasource(baseUrl + '/authentication');
    this.branch = new BranchDatasource(baseUrl + '/app/branch');
    this.stamp = new StampDatasource(baseUrl + '/app/stamp');
    this.news = new NewsDatasource(baseUrl + '/app/news');
    this.feedback = new FeedbackDatasource(baseUrl + '/app/feedback');
  }
}
