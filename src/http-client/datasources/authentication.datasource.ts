import {
  DtoLoginAnonymousRequest,
  DtoLoginResult,
  DtoLoginUserRequest,
  DtoRefreshResult,
  DtoRegisterAnonymousRequest,
  DtoRegisterCustomerRequest,
  DtoUser,
} from '../../dto';
import { BaseHttpClient } from '../base/base.http-client';

export class AuthenticationDatasource extends BaseHttpClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  public async login(data: DtoLoginUserRequest): Promise<DtoUser> {
    const loginResult: DtoLoginResult = await this.post<DtoLoginUserRequest, DtoLoginResult>('/login', data);
    this.setAuthData(loginResult.accessToken, loginResult.refreshToken);
    return loginResult.user;
  }

  public async loginAnonymous(data: DtoLoginAnonymousRequest): Promise<DtoUser> {
    const loginResult: DtoLoginResult = await this.post<DtoLoginAnonymousRequest, DtoLoginResult>('/login-anonymous', data);
    this.setAuthData(loginResult.accessToken, loginResult.refreshToken);
    return loginResult.user;
  }

  public async register(data: DtoRegisterCustomerRequest, loginAfterRegistration: boolean): Promise<DtoUser> {
    const registrationResult: DtoUser = await this.post<DtoRegisterCustomerRequest, DtoUser>('/register-customer', data);
    if (loginAfterRegistration) {
      const loginRequest: DtoLoginUserRequest = new DtoLoginUserRequest(data.email, data.password, data.deviceId);
      return await this.login(loginRequest);
    }
    return registrationResult;
  }

  public async registerAnonymous(data: DtoRegisterAnonymousRequest, loginAfterRegistration: boolean): Promise<DtoUser> {
    const registrationResult: DtoUser = await this.post<DtoRegisterAnonymousRequest, DtoUser>('/register-anonymous', data);
    if (loginAfterRegistration) {
      const loginRequest: DtoLoginAnonymousRequest = new DtoLoginAnonymousRequest(data.deviceId);
      return await this.loginAnonymous(loginRequest);
    }
    return registrationResult;
  }

  public async logout(): Promise<void> {
    await this.post<void, void>('/logout', null);
    this.clearAuthData();
  }

  public async refresh(): Promise<DtoUser> {
    const result = await this.get<DtoRefreshResult>('/refresh');
    this.setAuthData(result.accessToken);
    return result.user;
  }

  private setAuthData(accessToken: string = '', refreshToken: string = ''): void {
    if (accessToken != null && accessToken != '') {
      BaseHttpClient.setAuthToken(accessToken);
    }
    if (refreshToken != null && refreshToken != '') {
      BaseHttpClient.setRefreshToken(refreshToken);
    }
  }

  private clearAuthData(): void {
    BaseHttpClient.setAuthToken('');
    BaseHttpClient.setRefreshToken('');
  }
}
