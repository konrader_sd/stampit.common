import { DtoAddStampRequest, DtoAddStampResult } from '../../dto';
import { DtoFeedback } from '../../dto/feedback/feedback.dto';
import { BaseHttpClient } from '../base/base.http-client';

export class FeedbackDatasource extends BaseHttpClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  send(feedback: DtoFeedback): Promise<DtoFeedback> {
    return this.post<DtoFeedback, DtoFeedback>('', feedback);
  }
}
