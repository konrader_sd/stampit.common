import { DtoBranch } from '../../dto';
import { BaseHttpClient } from '../base/base.http-client';

export class BranchDatasource extends BaseHttpClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  public async getAllCompanies(): Promise<DtoBranch[]> {
    return this.get<DtoBranch[]>('/companies');
  }

  public async getAllShopsOfCompany(companyId: number): Promise<DtoBranch[]> {
    return this.get<DtoBranch[]>('/' + companyId + '/shops');
  }
}
