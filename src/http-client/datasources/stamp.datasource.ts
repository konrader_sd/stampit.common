import { DtoAddStampRequest, DtoAddStampResult } from '../../dto';
import { BaseHttpClient } from '../base/base.http-client';

export class StampDatasource extends BaseHttpClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  addStamp(request: DtoAddStampRequest): Promise<DtoAddStampResult> {
    return this.post<DtoAddStampRequest, DtoAddStampResult>('', request);
  }
}
