import { DtoNews } from '../../dto';
import { BaseHttpClient } from '../base/base.http-client';

export class NewsDatasource extends BaseHttpClient {
  constructor(baseUrl: string) {
    super(baseUrl);
  }

  findLatest(): Promise<DtoNews[]> {
    return this.get<DtoNews[]>('');
  }

  findByBranchId(branchId: number): Promise<DtoNews[]> {
    return this.get<DtoNews[]>(`/branch/${branchId}`);
  }
}
