export enum UserType {
  Anonymous,
  Customer,
  Employee,
}
