import { UserType } from '.';
import { DtoBranch } from '../branch/branch.dto';
import { ActiveState } from '../common';
import { DtoCustomerCard } from '../customer-card';

export class DtoUser {
  public id: number;
  public type: UserType;
  public state: ActiveState;
  public deviceId: string;
  public email: string;
  public branch: DtoBranch;
  public customerCards: DtoCustomerCard[];

  constructor(
    id: number = null,
    type: UserType = null,
    state: ActiveState = null,
    deviceId: string = '',
    email: string = '',
    branch: DtoBranch = null,
    customerCards: DtoCustomerCard[] = [],
  ) {
    this.id = id;
    this.type = type;
    this.state = state;
    this.deviceId = deviceId;
    this.email = email;
    this.branch = branch;
    this.customerCards = customerCards;
  }
}
