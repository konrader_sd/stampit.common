import { DtoCompanyCard } from '../company-card/company-card.dto';
import { DtoUser } from '../user/user.dto';
import { CustomerCardStatus } from './customer-card-status.enum';

export class DtoCustomerCard {
  public id: number;
  public companyCard: DtoCompanyCard;
  public customer: DtoUser;
  public stamps: number;
  public status: CustomerCardStatus;

  constructor(
    id: number = null,
    companyCard: DtoCompanyCard = null,
    customer: DtoUser = null,
    stamps: number = null,
    status: CustomerCardStatus = null,
  ) {
    this.id = id;
    this.companyCard = companyCard;
    this.customer = customer;
    this.stamps = stamps;
    this.status = status;
  }
}
