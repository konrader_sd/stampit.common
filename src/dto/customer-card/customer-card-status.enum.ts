export enum CustomerCardStatus {
  InProgress,
  Completed,
  Redeemed,
  Invalid,
}
