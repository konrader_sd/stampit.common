import { FeedbackType } from './feedback-type.enum';

export class DtoFeedback {
  public type: FeedbackType;
  public subject: string;
  public text: string;
  public email: string;

  constructor(type: FeedbackType, subject: string, text: string, email: string) {
    this.type = type;
    this.subject = subject;
    this.text = text;
    this.email = email;
  }
}
