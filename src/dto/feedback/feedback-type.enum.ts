export enum FeedbackType {
  Unspecified,
  FeatureRequest,
  BugReport,
  ReportAbuse,
}
