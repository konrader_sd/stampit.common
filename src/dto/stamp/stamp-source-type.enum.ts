export enum StampSourceType {
  QRCode,
  NFCTag,
}
