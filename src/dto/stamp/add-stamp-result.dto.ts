import { DtoCustomerCard } from '../customer-card/customer-card.dto';

export enum DtoAddStampResultType {
  Success,
  UnknownError,
  UnknownSourceType,
  InvalidPayload,
  InvalidCompanyCard,
}

export class DtoAddStampResult {
  resultType: DtoAddStampResultType;
  customerCards: DtoCustomerCard[];

  constructor(resultType: DtoAddStampResultType = null, customerCards: DtoCustomerCard[] = []) {
    this.resultType = resultType;
    this.customerCards = customerCards;
  }
}
