import { StampSourceType } from './stamp-source-type.enum';

export class DtoAddStampRequest {
  sourceType: StampSourceType;
  sourceId: number;
  payload: string;
}
