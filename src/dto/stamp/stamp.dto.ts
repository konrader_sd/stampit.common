import { DtoCustomerCard } from '../customer-card';
import { StampSourceType } from './stamp-source-type.enum';

export class DtoStamp {
  public id: number;
  public createdDate: Date;
  public customerCard: DtoCustomerCard;
  public sourceType: StampSourceType;
  public sourceId: number;
  public amount: number;

  constructor(
    id: number = null,
    createdDate: Date = null,
    customerCard: DtoCustomerCard = null,
    sourceType: StampSourceType = null,
    sourceId: number = null,
    amount: number = null,
  ) {
    this.id = id;
    this.createdDate = createdDate;
    this.customerCard = customerCard;
    this.sourceType = sourceType;
    this.sourceId = sourceId;
    this.amount = amount;
  }
}
