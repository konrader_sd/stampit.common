import { DtoBranch } from '../branch/branch.dto';
import { DtoCompanyCard } from '../company-card/company-card.dto';

export class DtoNfcDevice {
  public id: number;
  public name: string;
  public description: string;
  public branch: DtoBranch;
  public companyCard: DtoCompanyCard;
  public stampAmount: number;
  public uniqueIdentifier: string;

  constructor(
    id: number = null,
    name: string = '',
    description: string = '',
    branch: DtoBranch = null,
    companyCard: DtoCompanyCard = null,
    stampAmount: number = null,
    uniqueIdentifier: string = '',
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.branch = branch;
    this.companyCard = companyCard;
    this.stampAmount = stampAmount;
    this.uniqueIdentifier = uniqueIdentifier;
  }
}
