export class DtoLoginUserRequest {
  email: string;
  password: string;
  deviceId: string;

  constructor(email: string = '', password: string = '', deviceId: string = '') {
    this.email = email;
    this.password = password;
    this.deviceId = deviceId;
  }
}

export class DtoLoginAnonymousRequest {
  deviceId: string;

  constructor(deviceId: string = '') {
    this.deviceId = deviceId;
  }
}
