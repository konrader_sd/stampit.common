import { DtoUser } from '../user/user.dto';

export class DtoLoginResult {
  user: DtoUser;
  accessToken: string;
  refreshToken: string;
  type: LoginResultType;
}

export enum LoginResultType {
  Ok,
  Error,
}
