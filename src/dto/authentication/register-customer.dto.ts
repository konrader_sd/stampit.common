
export class DtoRegisterCustomerRequest {
  email: string;
  password: string;
  deviceId: string;
}
