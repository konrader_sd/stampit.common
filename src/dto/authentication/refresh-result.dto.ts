import { DtoUser } from '../user/user.dto';

export class DtoRefreshResult {
  user: DtoUser;
  accessToken: string;

  constructor(user: DtoUser = null, accessToken: string = '') {
    this.user = user;
    this.accessToken = accessToken;
  }
}
