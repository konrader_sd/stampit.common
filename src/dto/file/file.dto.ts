export class DtoFile {
  public width: number;
  public height: number;
  public url: string;

  constructor(width: number = null, height: number = null, url: string = '') {
    this.width = width;
    this.height = height;
    this.url = url;
  }
}
