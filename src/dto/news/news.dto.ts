import { DtoBranch } from '../branch';

export class DtoNews {
  public id: number;
  public title: string;
  public body: string;
  public branch: DtoBranch;
  public releaseDate: Date;

  constructor(id: number, title: string, body: string, branch: DtoBranch, releaseDate: Date) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.branch = branch;
    this.releaseDate = releaseDate;
  }
}
