export enum NewsState {
  Unknown,
  Hidden,
  Public,
}
