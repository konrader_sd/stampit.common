export enum BranchType {
  Root = 1,
  Company = 2,
  Pos = 3,
  Folder = 4,
}
