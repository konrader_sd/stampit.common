import { DtoAddress } from '../common/address.dto';
import { DtoCompanyCard } from '../company-card/company-card.dto';
import { DtoFile } from '../file/file.dto';
import { DtoNews } from '../news/news.dto';
import { DtoBranchCategory } from './branch-category.dto';
import { BranchType } from './branch-type.enum';

export class DtoBranch {
  public id: number;
  public name: string;
  public description: string;
  public type: BranchType;
  public parentId: number;
  public address: DtoAddress;
  public email: string;
  public mapsLink: string;
  public companyCards: DtoCompanyCard[];
  public categories: DtoBranchCategory[] = [];
  public news: DtoNews[] = [];
  public logoFile: DtoFile;
  public openingHours: string;

  constructor(
    id: number = null,
    name: string = '',
    description: string = '',
    type: BranchType = null,
    parentId: number = null,
    address: DtoAddress = null,
    email: string = '',
    mapsLink: string = '',
    companyCards: DtoCompanyCard[] = [],
    categories: DtoBranchCategory[] = [],
    news: DtoNews[] = [],
    logoFile: DtoFile = null,
    openingHours: string = ''
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.type = type;
    this.parentId = parentId;
    this.address = address;
    this.email = email;
    this.mapsLink = mapsLink;
    this.companyCards = companyCards;
    this.categories = categories;
    this.news = news;
    this.logoFile = logoFile;
    this.openingHours = openingHours;
  }
}
