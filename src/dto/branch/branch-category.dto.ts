export class DtoBranchCategory {
  public id: number;
  public name: string;
  public description: string;

  constructor(id: number = null, name: string = '', description: string = '') {
    this.id = id;
    this.name = name;
    this.description = description;
  }
}
