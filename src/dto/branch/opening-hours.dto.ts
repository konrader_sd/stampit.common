import { Weekday as Weekday } from './weekday.enum';

export class DtoOpeningHours {
  public day: Weekday;
  public from: Date;
  public to: Date;

  constructor(day: Weekday = null, from: Date = null, to: Date = null) {
    this.day = day;
    this.from = from;
    this.to = to;
  }
}
