export class DtoAddress {
  public id: number;
  public street: string;
  public number: string;
  public door: string;
  public postcode: string;
  public city: string;
  public country: string;

  constructor(
    id: number = null,
    street: string = '',
    number: string = '',
    door: string = '',
    postcode: string = '',
    city: string = '',
    country: string = '',
  ) {
    this.id = id;
    this.street = street;
    this.number = number;
    this.door = door;
    this.postcode = postcode;
    this.city = city;
    this.country = country;
  }
}
