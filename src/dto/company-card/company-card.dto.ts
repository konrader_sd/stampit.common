import { DtoBranch } from "../branch/branch.dto";
import { ActiveState } from "../common";

export class DtoCompanyCard {
  public id: number;
  public name: string;
  public description: string;
  public company: DtoBranch;
  public stampAmount: number;
  public status: ActiveState;

  constructor(
    id: number = null,
    name: string = '',
    description: string = '',
    company: DtoBranch = null,
    stampAmount: number = null,
    status: ActiveState = null,
  ) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.company = company;
      this.stampAmount = stampAmount;
      this.status = status;
  }
}
