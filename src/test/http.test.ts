import { FeedbackType } from '../dto/feedback/feedback-type.enum';
import { DtoFeedback } from '../dto/feedback/feedback.dto';
import { HttpClient } from '../http-client';

const httpClient = new HttpClient('http://localhost:3000');

async function test() {
  const rndDeviceId: string = generateRandomString(8);

  const registrationResult = await httpClient.authentication.registerAnonymous({ deviceId: rndDeviceId }, true);
  console.log('Login Result', registrationResult);

  const companies = await httpClient.branch.getAllCompanies();
  console.log('Companies Result', companies);

  const newAuthToken = await httpClient.authentication.refresh();
  console.log('Refresh Result', newAuthToken);

  const news = await httpClient.news.findLatest();
  console.log('News Result', news);

  const feedback = await httpClient.feedback.send(
    new DtoFeedback(FeedbackType.BugReport, 'nothing works', 'so buggy', 'dont@contact.me'),
  );
  console.log('feedback result', feedback);
}

function generateRandomString(length: number) {
  const result = [];
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
  }
  return result.join('');
}

test();
